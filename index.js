const chokidar = require('chokidar');
const axios = require('axios');
const fs = require('fs');
const qs =  require('querystring');
require('dotenv').config();

const watcher = chokidar.watch('./src', {persistent: true, ignored: /^\./, awaitWriteFinish: {
    stabilityThreshold: 2000,
    pollInterval: 100
  }});

watcher.on('change', function(path) {
    const value = getFileValue(path);
    log(`Changed file: ${path} (${value.length})`);
    sendChanges(path.split('src/')[1], value); 
});

console.log('Start watching. \n');

function sendChanges(path, value) {
    const cookie = getCookie();
    const data = {
        'asset[theme_id]': process.env.THEME_ID,
        'asset[key]': path,
        'asset[value]': value
    }
    
    axios({
        method: 'PUT',
        url: `https://brokevibes.myshopify.com/admin/themes/${process.env.THEME_ID}/assets`,
        headers: {
            'cookie': cookie,
            'x-csrf-token': process.env.CSRF_TOKEN,
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36',
            'x-shopify-web': '1'
        },
        data: qs.stringify(data)
    }).then(function(res) {
        if(res.status == 200) {
            log(`Success upload! ${path} \n`);
        } else {
            log(`Error! (${res.status}) ${path} \n`)
        }
    });

    
}

function getFileValue(path) {
    return fs.readFileSync(path, 'utf-8');
}

function getCookie() {
    return fs.readFileSync('config/cookie', 'utf-8');
}

function log(text) {
    const time = new Date().toString().split(' ')[4];
    console.log(`${time}: ${text}`);
}